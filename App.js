/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { Text, LogBox } from 'react-native';

import { navigationRef } from './app/navigation/rootNavigation';
import AuthNavigator from './app/navigation/AuthNavigator';
import AfterLogoutNavigator from './app/navigation/AfterLogoutNavigator';
import TabNavigator from './app/navigation/TabNavigator';
import settings from './app/config/settings';
import AuthContext from './app/auth/context';
import Itemcontext from './app/auth/itemcontext';
import authStorage from './app/auth/storage';
import SplashScreen from 'react-native-splash-screen';
import * as Sentry from '@sentry/react-native';
import { Root as ConfirmRoot, SPSheet } from 'react-native-popup-confirm-toast';
Sentry.init({
  dsn: 'https://2324931ac8ea4df29a590457c71c2dae@o1157842.ingest.sentry.io/6240535',
  integrations: [
    new Sentry.ReactNativeTracing({
      tracingOrigins: ['localhost', 'dev.veraticket.com', 'beta.veraticket.com', /^\//],
    }),
  ],
  // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
  // We recommend adjusting this value in production.
  tracesSampleRate: 1.0,
});

import client from './app/api/client';

LogBox.ignoreLogs([
  'Warning: ...',
  'You seem to update props of the "TRenderEngineProvider"',
  '[react-native-gesture-handler]',
  'Require cycle',
]); // Ignore log notification by message
LogBox.ignoreAllLogs();

const App = () => {
  let state = true;

  const [user, setUser] = useState(null);
  const [seletedItem, setseletedItem] = useState(null);

  const restoreUser = async () => {
    const user = await authStorage.getUser();
    const login = await authStorage.getLogin();
    let data = { ...user, login: login };
    const baseURL = await authStorage.getBaseURLOnly();
    client.setBaseURL(baseURL);
    if (data) setUser(await data);
  };
  useEffect(() => {
    restoreUser();
    setTimeout(() => {
      SplashScreen.hide();
    }, 1500);
  }, []);

  return (
    <NavigationContainer ref={navigationRef}>
      <ConfirmRoot>
        <AuthContext.Provider value={{ user, setUser }}>
          <Itemcontext.Provider value={{ seletedItem, setseletedItem }}>
            {(user && user?.login == 'true') || user?.login == true ? (
              <TabNavigator />
            ) : (user && user?.login == false) || user?.login == 'false' ? (
              <AfterLogoutNavigator />
            ) : (
              <AuthNavigator />
            )}
          </Itemcontext.Provider>
        </AuthContext.Provider>
      </ConfirmRoot>
    </NavigationContainer>
  );
};

export default Sentry.wrap(App);
