
# Scanner-Mobile-App

This React Native app is for the Vera ticket web app on which you can 
registor your self in an event going on and after registration the QR generated
which help customer to enter in the event to scan this scanner organiser of that
event use this app.

## :hammer: Build it yourself

Run the following commands:

```bash
# Clone this repo
git clone https://gitlab.com/systango/veraticket/mobile-app.git && cd mobile-app

# Install dependencies run following commands

yarn install 

# Install pod dependencies for IOS

npx pod-install ios

```

### Run the app

```bash
# Start metro server in your app folder 

npx react-native Start

# Run app on Android

npx react-native run-android

# Run app on IOS

npx react-native run-ios

```
