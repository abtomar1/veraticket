import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import MainBackground from '../components/MainBackground';
import AppButton from '../components/Button';
import AppText from '../components/Text';
import useAuth from '../auth/useAuth';
import storage from '../auth/storage';
import LottieView from 'lottie-react-native';
import AppPicker from '../components/UserPicker2';
import UserPickerItem from '../components/UserPickerItem';
import { ScrollView } from 'react-native-gesture-handler';

const AfterlogoutScreeen = ({ navigation, route }) => {
  const { logOut, restoreUserById } = useAuth();
  const [userDetails, setuserDetails] = useState({ name: '' });
  const [selectedItem, setSelectedItem] = useState(null);
  const [userlist, setuserlist] = useState(null);

  const buttonPressedOld = () => {
    restoreUserById(selectedItem);
  };
  const buttonPressedNew = () => {
    navigation.navigate('ScanQRScreen');
    logOut();
  };
  const getData = async () => {
    const userData = await storage.getUserDetails();
    setuserDetails(userData);
    const users = await storage.getUsersList();
    setuserlist(users);
    setSelectedItem(users[0]);
  };
  useEffect(() => {
    getData();
    return () => {};
  }, []);

  const separator = () => {
    return <View style={styles.separator} />;
  };

  return (
    <MainBackground>
      <ScrollView contentContainerStyle={{ flex: 1 }} style={{ flexGrow: 1 }}>
        {userlist && userlist?.length > 1 && (
          <View style={{ width: '100%' }}>
            <AppText style={{ fontSize: 12, color: '#FFFFFF' }}>Select User</AppText>
            <AppPicker
              items={userlist}
              onSelectItem={(item) => {
                setSelectedItem(item);
              }}
              PickerItemComponent={UserPickerItem}
              placeholder={'-- Select --'}
              selectedItem={selectedItem}
            />
          </View>
        )}
        <View style={styles.container}>
          <View style={{ marginVertical: '5%' }}>
            <LottieView
              style={{ width: 120, height: 120, alignSelf: 'center' }}
              source={require('../assets/animations/welcome2.json')}
              autoPlay
            />
          </View>
          <View style={styles.buttonContainer}>
            <AppText style={{ fontSize: 24, textAlign: 'center' }}>
              {`Welcome Back,\n ${selectedItem?.title}`}
            </AppText>
          </View>

          <View style={styles.buttonContainer}>
            <AppButton title={'Resume Session'} onPress={buttonPressedOld} />
          </View>
          {separator()}
          <View style={styles.buttonContainer}>
            <AppText style={{ fontSize: 24, textAlign: 'center' }}>
              {`Not ${selectedItem?.title}?`}
            </AppText>
          </View>
          <View style={styles.buttonContainer}>
            <AppButton
              color={'#fcfcfc'}
              textColor={'#000'}
              title={'Register as New User'}
              onPress={buttonPressedNew}
            />
          </View>
        </View>
      </ScrollView>
    </MainBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    marginVertical: '5%',
  },
  separator: {
    width: '100%',
    borderColor: '#4A4A4A',
    borderWidth: 1,
    marginHorizontal: 5,
    marginVertical: 20,
  },
});

export default AfterlogoutScreeen;
