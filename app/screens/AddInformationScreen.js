import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import MainBackground from '../components/MainBackground';
import colors from '../config/colors';
import { ErrorMessage, Form, FormField, SubmitButton } from '../components/forms';
import * as Yup from 'yup';
import CustomImagePicker from '../components/ImagePicker';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import store from '../auth/storage';
import listingsApi from '../api/listings';
import DeviceInfo from 'react-native-device-info';
import useAuth from '../auth/useAuth';
import Geolocation from 'react-native-geolocation-service';
import client from '../api/client';
import * as Sentry from '@sentry/react-native';
import { Popup } from 'react-native-popup-confirm-toast';
const validationSchema = Yup.object().shape({
  name: Yup.string()
    .min(4, 'Minimum 4 characters needed.')
    .required('Name cannot be empty')
    .matches(/^[a-zA-Z\s]*$/, 'Special characters and numbers not allowed')
    .label('Name'),
});

function AddInformationScreen({ navigation }) {
  const auth = useAuth();
  const [imageUrl, setImageUrl] = useState({ url: null });
  const [buttonLoading, setbuttonLoading] = useState(false);
  const [location, setLocation] = useState(null);
  const getData = async () => {
    let data = await store.getBaseURL();
    return data;
  };
  const handleSubmit = async (val, { setErrors }) => {
    let name = val.name.trim();
    if (name == '') {
      setErrors({ name: 'Space and special characters are not allowed.' });
      return;
    }
    setbuttonLoading(true);
    if (Platform.OS === 'android') {
      await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
    }

    const dataLocal = JSON.parse(JSON.parse(await getData()));
    Geolocation.getCurrentPosition(
      async (position) => {
        const { latitude, longitude } = position.coords;
        setLocation({
          latitude,
          longitude,
        });
        let payload2 = {
          deviceId: `${DeviceInfo.getUniqueId()}`,
          name: name,
          deviceName: `${await DeviceInfo.getDeviceName()}`,
          model: `${DeviceInfo.getModel()}`,
          os: `${Platform.OS}`,
          version: `${DeviceInfo.getVersion()}`,
          passcode: `${dataLocal?.passcode}`,
          organizerId: dataLocal?.organizerId,
          lat: latitude,
          lng: longitude,
        };
        try {
          const result = await listingsApi.registartion(JSON.stringify(payload2));
          if (result?.ok) {
            auth.storeUser(result?.data?.data);
          } else {
            Sentry.captureException(result?.data?.message);
            Popup.show({
              type: 'danger',
              title: 'Error',
              textBody: 'SERVER_ERROR: Somthing went wrong!',
              buttonText: 'Ok',
              callback: () => Popup.hide(),
              okButtonStyle: {
                backgroundColor: colors.redButtonColor,
              },
            });
          }
        } catch (error) {
          Sentry.captureException(error);
          console.log('Error ', error);
        }
        setbuttonLoading(false);
      },
      (error) => {
        console.log(error.code, error.message);
        setbuttonLoading(false);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  };

  useEffect(() => {
    Geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        setLocation({
          latitude,
          longitude,
        });
      },
      (error) => {
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }, []);
  return (
    <MainBackground>
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
        style={{ height: 50, width: 50, left: -20, top: 20 }}
      >
        <Ionicons name={'chevron-back'} size={24} color={colors.white} />
      </TouchableOpacity>

      <View style={styles.mainContainer}>
        <View>
          <Form
            initialValues={{ name: '' }}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
          >
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              name="name"
              topTitle="Name"
              placeholder="e.g. John Doe"
            />

            <View style={styles.buttonBox}>
              <SubmitButton
                buttonLoading={buttonLoading}
                title="Submit"
                style={styles.submitButtonText}
              />
            </View>
          </Form>
        </View>
      </View>
    </MainBackground>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  buttonBox: {
    marginTop: '5%',
  },
  submitButtonText: {
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 19,
    textAlign: 'center',
    color: colors.white,
  },
});

export default AddInformationScreen;
