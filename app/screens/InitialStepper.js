import React, { useState, useRef, useEffect } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, Animated } from 'react-native';
import MainBackground from '../components/MainBackground';
import AppText from '../components/Text';
import { useNavigation } from '@react-navigation/native';
import Swiper from 'react-native-swiper';
import colors from '../config/colors';
import LottieView from 'lottie-react-native';

function InitialStepper({}) {
  const navigation = useNavigation();
  const refStepper = useRef(null);
  const [index, setindex] = useState(0);
  const buttonPressed = () => {
    try {
      const routes = navigation.getState().routeNames;
      if (routes.includes('AfterlogoutScreeen')) {
        navigation.navigate('AfterlogoutScreeen');
      } else {
        navigation.navigate('ScanQRScreen');
      }
    } catch (error) {
      console.log('error ', error);
    }
  };

  const buttonPressedBack = () => {
    setindex(refStepper.current?.state?.index - 1);
  };

  const Button = ({ onPress, title }) => {
    return (
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <Text style={styles.textButton}>{title}</Text>
      </TouchableOpacity>
    );
  };

  const indexChange = (index) => {
    setindex(index);
  };

  const firstScreen = () => {
    return (
      <React.Fragment>
        <View style={styles.container}>
          <View style={styles.innerFlex}>
            <LottieView
              style={{ width: 150, height: 150 }}
              source={require('../assets/animations/sending-mail.json')}
              autoPlay
            />
          </View>
          <View style={[styles.innerFlex, { flex: 2, justifyContent: 'center' }]}>
            <AppText style={styles.heading}>Generate QR code</AppText>
          </View>
          <View style={[styles.innerFlex, { flex: 4, width: '80%' }]}>
            <AppText style={styles.subHeading}>
              Organizers will generate QR and share with you or send it via email.
            </AppText>
          </View>
          <View>
            <Button
              style={styles.button}
              title={'Next'}
              onPress={() => {
                setindex(refStepper.current?.state?.index + 1);
              }}
            />
          </View>
        </View>
      </React.Fragment>
    );
  };

  const secondScreen = () => {
    return (
      <React.Fragment>
        <View style={styles.container}>
          <View style={styles.innerFlex}>
            <LottieView
              style={{ width: 150, height: 150 }}
              source={require('../assets/animations/scanner.json')}
              autoPlay
            />
          </View>
          <View style={[styles.innerFlex, { flex: 2, justifyContent: 'center' }]}>
            <AppText style={styles.heading}>Scan QR code</AppText>
          </View>
          <View style={[styles.innerFlex, { flex: 4, width: '80%' }]}>
            <AppText style={styles.subHeading}>
              Scan the received QR and enter your name, and you are ready to scan!
            </AppText>
          </View>
          <View
            style={{
              justifyContent: 'space-evenly',
              flexDirection: 'row',
              width: '90%',
            }}
          >
            <Button style={styles.button} title={'Back'} onPress={buttonPressedBack} />
            <Button style={styles.button} title={'Done'} onPress={buttonPressed} />
          </View>
        </View>
      </React.Fragment>
    );
  };

  return (
    <MainBackground>
      <Swiper
        ref={refStepper}
        style={styles.wrapper}
        showsButtons={false}
        loop={false}
        scrollEnabled={false}
        index={index}
        onIndexChanged={indexChange}
        activeDot={<View style={styles.active} />}
      >
        <View style={styles.slide1}>{firstScreen()}</View>
        <View style={styles.slide2}>{secondScreen()}</View>
      </Swiper>
    </MainBackground>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    marginHorizontal: '20%',
    flexDirection: 'column',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingVertical: '30%',
  },
  wrapper: { flexGrow: 1 },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  innerFlex: { flex: 4, marginVertical: '2%' },
  button: {
    width: 100,
    backgroundColor: colors.redButtonColor,
    borderRadius: 100,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButton: {
    color: colors.white,
    textTransform: 'none',
    lineHeight: 19,
    textAlign: 'center',
    fontWeight: 'bold',
    fontStyle: 'normal',
  },
  active: {
    backgroundColor: colors.redButtonColor,
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3,
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  heading: {
    color: '#fff',
    fontSize: 24,
    lineHeight: 28,
    fontWeight: 'bold',
  },
  subHeading: {
    lineHeight: 23,
    fontSize: 14,
    textAlign: 'center',
    fontStyle: 'normal',
  },
});

export default InitialStepper;
