import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import MainBackground from '../components/MainBackground';
import AddInfo from '../components/AddInfo';
import ActivityIndicator from '../components/ActivityIndicator';
import listingsApi from '../api/listings';
import store from '../auth/storage';
import * as Sentry from '@sentry/react-native';
import { Popup } from 'react-native-popup-confirm-toast';
import colors from '../config/colors';

function ProfileScreen({ navigation }) {
  const [loading, setloading] = useState(true);
  const [buttonLoading, setbuttonLoading] = useState(false);
  const [Data, setData] = useState({ name: '' });

  const getbaseURL = async () => {
    let data = await store.getBaseURL();
    return data;
  };
  onSubmit = async (value) => {
    setbuttonLoading(true);
    let baseURLData = JSON.parse(JSON.parse(await getbaseURL()));
    try {
      const authToken = JSON.parse(await store.getToken());
      const userData = await store.getUserDetails();
      let payload = {
        name: value.name,
      };

      const result = await listingsApi.updateProfileData(
        baseURLData.baseUrl,
        authToken?.id,
        JSON.stringify(payload)
      );

      if (result?.ok) {
        if (result.data?.code == 200) {
          let Data = { ...userData, name: value.name };
          await store.updateUsersList(authToken?.organizerId, value.name);
          await store.storeUserDetails(Data);
          Popup.show({
            type: 'success',
            title: result.data?.message,
            buttonText: 'Ok',
            callback: () => Popup.hide(),
            okButtonStyle: {
              backgroundColor: colors.redButtonColor,
            },
          });
        }
      } else {
        Sentry.captureException(result.data?.message);
      }
    } catch (error) {
      Sentry.captureException(error);
      console.log('Error ', error);
    }
    setbuttonLoading(false);
  };

  const getprofile = async () => {
    setloading(true);
    let baseURLData = JSON.parse(JSON.parse(await getbaseURL()));
    try {
      const authToken = JSON.parse(await store.getToken());
      const result = await listingsApi.getProfileData(baseURLData.baseUrl, authToken?.id);
      if (result?.ok) {
        if (result?.data?.code == 200) {
          let Data = { ...result?.data?.data, login: true };

          await store.updateUsersList(result?.data?.data?.organizerId, result?.data?.data?.name);
          await store.storeUserDetails(Data);
          setData({ name: result?.data?.data?.name });
        }
      } else {
        Sentry.captureException(result.data?.message);
        // alert('Something went wrong!');
      }
    } catch (error) {
      Sentry.captureException(error);
      console.log('Error ', error);
    }
    setloading(false);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getprofile();
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    getprofile();
  }, []);

  return loading ? (
    <ActivityIndicator visible={loading} />
  ) : (
    <MainBackground>
      <AddInfo
        onSubmit={(value) => {
          onSubmit(value);
        }}
        info={Data}
        buttonLoading={buttonLoading}
      />
    </MainBackground>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
});

export default ProfileScreen;
