import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import ImageBackground from '../../components/ImageBackground';
import AppButton from '../../components/Button';
import AppText from '../../components/Text';

const EventScreen = ({ navigation, route }) => {
  const Data = route.params.data;
  const buttonPressed = () => {
    navigation.navigate('EventScannerScreen');
  };
  return (
    <ImageBackground Data={Data} route={route} title={'Scan'}>
      <View style={styles.innerContainer}>
        <AppText style={styles.eventTitle}>{Data.title}</AppText>
        <AppButton onPress={buttonPressed} title={'Scan Now'} style={styles.appButton} />
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  appButton: { fontWeight: 'bold', color: '#fcfcfc', fontSize: 18 },
  innerContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    paddingTop: '40%',
  },
  eventTitle: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 20,
    lineHeight: 24,
    textAlign: 'center',
    width: '70%',
    marginVertical: '10%',
  },
});

export default EventScreen;
