import React, { useState, useRef, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, SafeAreaView, ActivityIndicator } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import colors from '../../config/colors';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import AppText from '../../components/Text';
import listingsApi from '../../api/listings';
import store from '../../auth/storage';
import Geolocation from 'react-native-geolocation-service';
import * as Sentry from '@sentry/react-native';
import { useIsFocused } from '@react-navigation/native';
import { Root, Popup } from 'react-native-popup-confirm-toast';

function EventScannerScreen({ navigation, route }) {
  const isFocused = useIsFocused();
  const eventData = route?.params?.data;
  const refScanner = useRef(null);
  const [loading, setloading] = useState(false);
  const [location, setLocation] = useState(null);
  const getbaseURL = async () => {
    let data = await store.getBaseURL();
    return data;
  };
  useEffect(() => {
    if (isFocused) {
      refScanner.current.reactivate();
    }
    return () => {};
  }, [isFocused]);

  const getData = async (data) => {
    const baseURLData = JSON.parse(JSON.parse(await getbaseURL()));
    try {
      setloading(true);
      const result = await listingsApi.getQRDetails(baseURLData.baseUrl, eventData.id, data);
      if (result?.ok) {
        navigation.navigate('EventDetailScreen', { data: result?.data });
      } else {
        if (result?.data?.statusCode == 401) {
          Popup.show({
            type: 'danger',
            title: 'Your device is not approved by organiser.',
            buttonText: 'Ok',
            callback: () => Popup.hide(),
            okButtonStyle: {
              backgroundColor: colors.redButtonColor,
            },
          });
        } else if (result?.data?.statusCode == 403) {
          Popup.show({
            type: 'danger',
            title: result?.data?.message,
            buttonText: 'Ok',
            callback: () => Popup.hide(),
            okButtonStyle: {
              backgroundColor: colors.redButtonColor,
            },
          });
        } else if (result?.data?.statusCode == 500) {
          Popup.show({
            type: 'danger',
            title: 'Alert',
            textBody: 'Something went wrong.',
            buttonText: 'Ok',
            callback: () => Popup.hide(),
            okButtonStyle: {
              backgroundColor: colors.redButtonColor,
            },
          });
        } else {
          Popup.show({
            type: 'danger',
            title: result?.data?.message,
            buttonText: 'Ok',
            callback: () => Popup.hide(),
            okButtonStyle: {
              backgroundColor: colors.redButtonColor,
            },
          });
        }
        navigation.goBack();
      }
    } catch (error) {
      Sentry.captureException(error);
      console.log('error ', error);
    }
    setloading(false);
  };

  const onSuccess = async (value) => {
    var Data = await JSON.parse(value?.data);
    Geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        setLocation({
          latitude,
          longitude,
        });
        const newData = {
          id: Data?.id,
          seat: Data?.seat,
          lat: latitude,
          lng: longitude,
        };
        if (isFocused) {
          getData(newData);
        }
      },
      (error) => {
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  };

  useEffect(() => {
    Geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        setLocation({
          latitude,
          longitude,
        });
      },
      (error) => {
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }, []);

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: 'black' }}>
      {loading ? (
        <View style={styles.loadView}>
          <ActivityIndicator size={'large'} color={colors.white} />
        </View>
      ) : null}
      <View style={styles.mainCon}>
        <TouchableOpacity
          disabled={loading}
          style={{ position: 'absolute', left: 20 }}
          onPress={() => {
            navigation.goBack();
          }}
        >
          <Ionicons name={'chevron-back'} size={24} color={colors.white} />
        </TouchableOpacity>
        <AppText style={{ fontSize: 24 }}>Scan</AppText>
      </View>

      <View
        style={{
          flex: 1,
        }}
      >
        <QRCodeScanner
          ref={refScanner}
          showMarker={true}
          reactivate={false}
          markerStyle={{ borderColor: colors.white, borderStyle: 'dashed' }}
          cameraStyle={styles.camStyle}
          cameraContainerStyle={styles.camStyle}
          onRead={onSuccess}
        ></QRCodeScanner>
        <View style={styles.headerView}></View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  camStyle: {
    height: '100%',
    width: '100%',
  },
  loadView: { position: 'absolute', top: '55%', zIndex: 10, left: '45%' },
  mainCon: {
    width: '100%',
    height: 60,
    paddingHorizontal: 20,
    top: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  headerView: {
    flex: 1,
    zIndex: 1,
    top: 50,
    position: 'absolute',
    height: 50,
    width: '100%',
  },
});

export default EventScannerScreen;
