import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import ImageBackground from '../../components/ImageBackground2';
import AppButton from '../../components/Button';
import AppText from '../../components/Text';
import colors from '../../config/colors';
import LinearGradient from 'react-native-linear-gradient';
import LottieView from 'lottie-react-native';

const EventDetailScreen = ({ navigation, route }) => {
  const data = route?.params?.data;
  const [imageloading, setimageloading] = useState(false);

  const DetailTextTop = ({ title, sub }) => {
    return (
      <View style={styles.box}>
        <AppText style={[styles.text, styles.detailTitle]}>{title}</AppText>
        <AppText
          style={[styles.middledot, { fontSize: 24, fontWeight: 'bold', lineHeight: 24, flex: 1 }]}
        >{`:`}</AppText>
        <AppText style={[styles.text, styles.detailsub]}>{sub}</AppText>
      </View>
    );
  };

  return (
    <ImageBackground
      onLoadStarti={(value) => {
        setimageloading(value);
      }}
      onLoadEndi={(value) => {
        setimageloading(value);
      }}
      Data={`${data?.metaDetails?.domainUrl}${data?.data?.eventImage}`}
      title={'Detail'}
    >
      <ScrollView>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <LottieView
            style={{ width: 120, height: 120 }}
            source={require('../../assets/animations/confirmt.json')}
            autoPlay
          />
        </View>
        <LinearGradient
          colors={['#000', 'transparent']}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 0 }}
          style={styles.line}
        >
          <View style={styles.innerContainer}>
            <AppText style={styles.eventTitle}>{data?.data?.eventName}</AppText>

            <DetailTextTop title={'QUANTITY ALLOWED '} sub={`${data?.data?.quantity}`} />

            <View style={styles.rowView}>
              <AppText style={styles.rowLeftView}>{`SEAT NUMBER`}</AppText>
              <AppText style={styles.middledot}>{`:`}</AppText>

              <View style={styles.rowRightView}>
                <AppText style={styles.rowRightViewInner}>{`${data?.data?.seatName}`}</AppText>
              </View>
            </View>

            <View style={styles.rowView}>
              <AppText style={styles.rowLeftView}>{`CATEGORY`}</AppText>
              <AppText style={styles.middledot}>{`:`}</AppText>
              <View style={styles.rowRightView}>
                <AppText style={styles.rowRightViewInner}>{`${data?.data?.category}`}</AppText>
              </View>
            </View>
          </View>
        </LinearGradient>
      </ScrollView>
      <View style={styles.buttonView}>
        <AppButton
          onPress={() => {
            navigation.goBack();
          }}
          title={'Scan Next'}
          style={styles.button}
        />
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  detailsub: {
    color: colors.white,
    fontSize: 24,
    fontWeight: 'bold',
    lineHeight: 24,
    flex: 4,
  },
  button: { fontWeight: 'bold', color: '#fcfcfc', fontSize: 18 },
  buttonView: { alignItems: 'center', top: '3%', alignSelf: 'center' },
  detailTitle: {
    color: colors.white,
    fontWeight: 'bold',
    fontSize: 20,
    lineHeight: 24,
    flex: 10,
  },
  line: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    marginTop: '25%',
  },
  rowView: {
    flexDirection: 'row',
    width: '100%',
    marginTop: '5%',
    borderColor: colors.white,
    borderBottomWidth: 2,
    marginVertical: '1%',
    minHeight: 60,
    maxHeight: 400,
    alignItems: 'center',
  },
  middledot: {
    flex: 0.5,
    fontSize: 14,
    color: '#fcfcfc',
    fontWeight: '600',
    lineHeight: 16,
  },
  rowRightView: { flex: 8, flexGrow: 8 },
  rowRightViewInner: {
    color: '#fcfcfc',
    fontWeight: '600',
    lineHeight: 16,
    fontSize: 14,
    textAlign: 'left',
  },
  rowLeftView: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 16,
    textAlign: 'left',
    flex: 4,
  },
  innerContainer: {
    alignItems: 'flex-start',
    flex: 1,
    width: '90%',
    justifyContent: 'center',
    flexDirection: 'column',
    paddingHorizontal: '5%',
  },
  eventTitle: {
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 28,
    lineHeight: 28,
    textAlign: 'left',
    width: '85%',
    marginVertical: '1%',
  },
  eventDescription: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 18,
    lineHeight: 24,
    textAlign: 'left',
    width: '85%',
    marginVertical: '1%',
  },
  text: {
    fontWeight: 'bold',
    fontSize: 13,
    lineHeight: 16,
    flex: 6,
  },
  box: {
    flexGrow: 1,
    borderColor: colors.white,
    borderBottomWidth: 2,
    width: '100%',
    height: 60,
    marginVertical: '1%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: '1%',
    alignItems: 'center',
  },
  text2: {
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 16,
    color: colors.white,
  },
});

export default EventDetailScreen;
