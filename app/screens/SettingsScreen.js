import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Alert, TouchableOpacity } from 'react-native';
import MainBackground from '../components/MainBackground';
import AppText from '../components/Text';
import useAuth from '../auth/useAuth';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import colors from '../config/colors';
import DeviceInfo from 'react-native-device-info';
import EnvironmentSwitcher from '../components/EnvironmentSwitcher';
import { Popup } from 'react-native-popup-confirm-toast';

function SettingsScreen({ navigation }) {
  const { user, logOut, softlogOut, deleteUserById } = useAuth();
  const [version, setversion] = useState(DeviceInfo.getVersion());
  const [showEnvironmentSwitcher, setShowEnvironmentSwitcher] = useState(false);
  const [tapCount, setTapCount] = useState(0);

  const confirmLogout = () => {
    Popup.show({
      type: 'confirm',
      textBody: 'Are you sure you want to logout?',
      buttonText: 'Yes',
      confirmText: 'No',
      modalContainerStyle: styles.modal,
      okButtonStyle: {
        backgroundColor: colors.redButtonColor,
      },
      callback: () => {
        softlogOut();
        Popup.hide();
      },
      cancelCallback: () => {
        Popup.hide();
      },
    });
  };

  const deleteUser = () => {
    Popup.show({
      type: 'confirm',
      textBody: `Are you sure you want to delete this account?`,
      buttonText: 'Yes',
      confirmText: 'No',
      modalContainerStyle: {
        width: '80%',
        backgroundColor: '#fff',
        borderRadius: 20,
        alignItems: 'center',
        overflow: 'hidden',
        position: 'absolute',
      },
      okButtonStyle: {
        backgroundColor: colors.redButtonColor,
      },
      callback: () => {
        deleteUserById(user?.organizerId);
        Popup.hide();
      },
      cancelCallback: () => {
        Popup.hide();
      },
    });
  };

  const onVersionPress = () => {
    const newTapCount = tapCount + 1;
    if (newTapCount >= 7) {
      setShowEnvironmentSwitcher(true);
    } else {
      setTapCount(newTapCount);
    }
  };

  const closeEnvironmentSwitchModal = () => {
    setShowEnvironmentSwitcher(false);
    setTapCount(0);
  };

  return (
    <>
      <MainBackground centerItem={false}>
        <View style={{ flexGrow: 1, paddingVertical: '5%' }}>
          <TouchableOpacity onPress={() => deleteUser()} style={styles.head}>
            <MaterialCommunityIcons name="delete" color="#FF7D5C" size={30} />
            <AppText style={[styles.title, { color: colors.white, left: 10 }]} numberOfLines={1}>
              De-register account
            </AppText>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => confirmLogout()} style={styles.head}>
            <MaterialCommunityIcons name="logout" color="#FF7D5C" size={30} />

            <AppText style={[styles.title, { color: colors.white, left: 10 }]} numberOfLines={1}>
              Logout
            </AppText>
          </TouchableOpacity>
        </View>
      </MainBackground>
      <View style={styles.bottomView}>
        <TouchableOpacity onPress={onVersionPress}>
          <AppText style={[styles.title, { color: colors.white, left: 10 }]} numberOfLines={1}>
            V {version}
          </AppText>
        </TouchableOpacity>
      </View>
      {showEnvironmentSwitcher && (
        <EnvironmentSwitcher
          modalVisible={showEnvironmentSwitcher}
          setShowEnvironmentSwitcher={closeEnvironmentSwitchModal}
        />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  head: {
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: colors.buttonBackground,
    padding: 10,
    marginVertical: '2%',
  },
  modal: {
    width: '80%',
    backgroundColor: '#fff',
    borderRadius: 20,
    alignItems: 'center',
    overflow: 'hidden',
    position: 'absolute',
  },
  detailsContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    fontSize: 16,
    textAlign: 'right',
  },
  bottomView: {
    position: 'absolute',
    bottom: 20,
    right: 40,
  },
});

export default SettingsScreen;
