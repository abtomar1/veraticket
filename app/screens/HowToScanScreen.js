import React, { useState } from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import MainBackground from '../components/MainBackground';
import AppText from '../components/Text';
import AppButton from '../components/Button';
import { useNavigation } from '@react-navigation/native';

function HowToScanScreen({}) {
  const navigation = useNavigation();

  const buttonPressed = () => {
    try {
      const routes = navigation.getState().routeNames;
      if (routes.includes('AfterlogoutScreeen')) {
        navigation.navigate('AfterlogoutScreeen');
      } else {
        navigation.navigate('ScanQRScreen');
      }
    } catch (error) {
      console.log('error ', error);
    }
  };

  return (
    <MainBackground>
      <View style={styles.mainContainer}>
        <View style={styles.container}>
          <Image
            style={styles.image}
            source={require('../assets/Phone.png')}
            resizeMode="stretch"
          />
        </View>
        <View>
          <View style={styles.HeadingcontainerView}>
            <AppText style={styles.Heading}>How to scan QR code</AppText>
          </View>
          <View style={styles.subHeadingView}>
            <AppText style={styles.subHeading}>
              QR code scanner lets you use phone’s camera to recognize barcodes and QR code.{'\n'}
              Make sure the code is fully within the detection box that shows in your camera view.
            </AppText>
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <AppButton title={'Got it'} onPress={buttonPressed} />
        </View>
      </View>
    </MainBackground>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    marginHorizontal: '10%',
    flexDirection: 'column',
  },
  container: { marginTop: '40%' },
  subHeadingView: {},
  subHeading: {
    lineHeight: 23,
    fontSize: 14,
    textAlign: 'center',
    fontStyle: 'normal',
  },
  HeadingcontainerView: {
    marginVertical: '10%',
  },
  Heading: {
    fontSize: 24,
    lineHeight: 28,
    textAlign: 'center',
  },
  image: {
    width: 70.25,
    height: 135.37,
  },
  buttonContainer: {
    marginVertical: '10%',
  },
});

export default HowToScanScreen;
