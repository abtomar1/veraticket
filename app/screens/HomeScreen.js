import React, { useState, useEffect, useCallback } from 'react';
import {
  View,
  StyleSheet,
  RefreshControl,
  Image,
  ScrollView,
  ActivityIndicator as ActivityIndicatorNative,
  Text,
} from 'react-native';
import MainBackground from '../components/MainBackground2';
import AppPicker from '../components/UserPicker';
import UserPickerItem from '../components/UserPickerItem';
import AppText from '../components/Text';
import colors from '../config/colors';
import CircularProgress from '../components/progressBar/index';
import ActivityIndicator from '../components/ActivityIndicator';
import listingsApi from '../api/listings';
import store from '../auth/storage';
import useAuth from '../auth/useAuth';
import * as Sentry from '@sentry/react-native';
import { useIsFocused } from '@react-navigation/native';
import LottieView from 'lottie-react-native';
import { Popup } from 'react-native-popup-confirm-toast';
import dashboard from '../components/downloadFile/index';
import DeviceInfo from 'react-native-device-info';

function HomeScreen({ navigation }) {
  const auth = useAuth();
  const [buildNumber, setbuildNumber] = useState(DeviceInfo.getBuildNumber());
  const [deviceStatus, setdeviceStatus] = useState(false);
  const { checkPermission } = dashboard();
  const isFocused = useIsFocused();
  const [selectedItem, setSelectedItem] = useState(null);
  const [data, setData] = useState([]);
  const [cardData, setcardData] = useState(null);
  const [loading, setloading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [itemLoading, setitemLoading] = useState(false);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setSelectedItem(auth.seletedItem);
    });
    return unsubscribe;
  }, [navigation, auth.seletedItem]);

  const getbaseURL = async () => {
    let data = await store.getBaseURL();
    return data;
  };

  const getEventListRefresh = async () => {
    let baseURLData = JSON.parse(JSON.parse(await getbaseURL()));
    try {
      const result = await listingsApi.getEventData(baseURLData.baseUrl);
      if (result?.ok) {
        let eventList = [];
        result.data?.data?.rows.map((val) => {
          let uri = val?.eventImage
            ? result.data?.metaDetails?.domainUrl + val?.eventImage
            : 'https://i.picsum.photos/id/987/536/354.jpg?hmac=qMAJC_SCI2Tp7gCUYRyTsWE2rVFqGajrxQKTFCkg-FI';
          eventList.push({
            id: val.id,
            userId: val.userId,
            title: val.name,
            about: val.about,
            venue: val.venue,
            uri: uri,
            startDate: val.startDate,
            venue: val.venue,
            about: val.about,
          });
        });
        setData(eventList);
      } else {
        Sentry.captureException(result.data?.message);
      }
    } catch (error) {
      Sentry.captureException(error);
      console.log('Error ', error);
    }
  };
  const getEventList = async () => {
    let baseURLData = JSON.parse(JSON.parse(await getbaseURL()));
    try {
      const result = await listingsApi.getEventData(baseURLData.baseUrl);
      if (result?.ok) {
        let eventList = [];
        result.data?.data?.rows.map((val) => {
          let uri = val?.eventImage
            ? result.data?.metaDetails?.domainUrl + val?.eventImage
            : 'https://i.picsum.photos/id/987/536/354.jpg?hmac=qMAJC_SCI2Tp7gCUYRyTsWE2rVFqGajrxQKTFCkg-FI';
          eventList.push({
            id: val.id,
            userId: val.userId,
            title: val.name,
            about: val.about,
            venue: val.venue,
            uri: uri,
            startDate: val.startDate,
            venue: val.venue,
            about: val.about,
          });
        });
        setData(eventList);
        setSelectedItem(eventList[0]);
        auth.setItem(eventList[0]);
        setTimeout(() => {
          setloading(false);
        }, 500);
      } else {
        Sentry.captureException(result.data?.message);
      }
    } catch (error) {
      Sentry.captureException(error);
      console.log('Error ', error);
    }
  };

  const getEventAnalytics = async () => {
    setitemLoading(true);
    let baseURLData = JSON.parse(JSON.parse(await getbaseURL()));

    try {
      const result = await listingsApi.getEventAnalytics(baseURLData.baseUrl, selectedItem?.id);

      if (result?.ok) {
        if (result.data?.data) {
          setcardData(result.data?.data);
        } else {
          console.log(result?.data?.message);
        }
      } else {
        console.log(result?.data?.message);
      }
    } catch (error) {
      Sentry.captureException(error);
      console.log('Error ', error);
    }
    setitemLoading(false);
  };

  const downloadApp = (url) => {
    Popup.show({
      type: 'warning',
      title: 'New version of app is here download it now!',
      buttonText: 'Ok',
      modalContainerStyle: {
        width: '80%',
        backgroundColor: '#fff',
        borderRadius: 20,
        alignItems: 'center',
        overflow: 'hidden',
        position: 'absolute',
      },
      okButtonStyle: {
        backgroundColor: colors.redButtonColor,
      },
      callback: async () => {
        let response = checkPermission(url);
        Popup.hide();
      },
    });
  };

  const getAppLink = async () => {
    try {
      const result = await listingsApi.getAppLink();

      if (result?.ok) {
        if (result?.data?.code == 200) {
          downloadApp(result?.data?.data?.appUrl);
        }
      }
    } catch (error) {
      console.log('Error ', error);
    }
  };

  const getCurrentAppVersion = async () => {
    try {
      const result = await listingsApi.getCurrentAppVersion();

      if (result?.ok) {
        if (result?.data?.code == 200) {
          let appbuildNumber = parseInt(buildNumber);
          let apibuildNumber = parseInt(result?.data?.data?.buildNumber);
          if (appbuildNumber != apibuildNumber) {
            getAppLink();
          }
        }
      }
    } catch (error) {
      Sentry.captureException(error);
      console.log('Error ', error);
    }
  };

  const getDevciesStatus = async () => {
    try {
      const result = await listingsApi.getDeviceStatus();

      if (result?.ok) {
        if (result?.data?.code == 200) {
          if (result?.data?.data?.deviceStatus == 'ACTIVE') {
            setdeviceStatus(true);
          } else {
            setdeviceStatus(false);
          }
        }
      }
    } catch (error) {
      Sentry.captureException(error);
      console.log('Error ', error);
    }
  };

  useEffect(() => {
    getEventList();
  }, []);

  useEffect(() => {
    getEventAnalytics();
  }, [selectedItem, isFocused]);

  useEffect(() => {
    if (isFocused) {
      getCurrentAppVersion();
      getDevciesStatus();
    }
  }, [isFocused]);

  const separator = () => {
    return (
      <View
        style={{
          height: 50,
          borderColor: '#4A4A4A',
          borderWidth: 1,
          marginHorizontal: 5,
        }}
      />
    );
  };

  const cardText = ({ cardTitle, subtitle, textLarge = false }) => {
    if (textLarge) {
      return (
        <View style={styles.cardInnerViewText}>
          <AppText style={styles.cardTitleLarge}>{cardTitle}</AppText>
          <AppText style={styles.cardSubTitleLarge}>{subtitle}</AppText>
        </View>
      );
    }
    return (
      <View style={styles.cardInnerViewText}>
        <AppText style={styles.cardTitle}>{cardTitle}</AppText>
        <AppText style={styles.cardSubTitle}>{subtitle}</AppText>
      </View>
    );
  };

  const Card = ({ cardName, per, title, subtitle, title2, subtitle2, textLarge }) => {
    return (
      <>
        <AppText style={styles.tag}>{cardName}</AppText>
        <View style={styles.cardView}>
          <View style={styles.cardInnerView}>
            <CircularProgress
              value={per}
              inActiveStrokeColor={'#2ecc71'}
              inActiveStrokeOpacity={0.2}
              textColor={'#fff'}
              valueSuffix={'%'}
            />
          </View>
          {title &&
            cardText({
              cardTitle: title,
              subtitle: subtitle,
            })}
          {title && separator()}
          {cardText({
            cardTitle: title2,
            subtitle: subtitle2,
          })}
        </View>
      </>
    );
  };
  const onRefresh = useCallback(() => {
    getEventListRefresh();
    getEventAnalytics();
    getDevciesStatus();
  }, []);

  return loading ? (
    <ActivityIndicator visible={loading} />
  ) : data.length == 0 ? (
    <MainBackground>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: '40%',
        }}
      >
        <LottieView
          style={{ width: 150, height: 150 }}
          source={require('../assets/animations/empty.json')}
          autoPlay
        />
        <AppText style={{ fontSize: 18, color: '#FFFFFF', fontWeight: 'bold' }}>
          Currenly you have no event
        </AppText>
      </View>
    </MainBackground>
  ) : (
    <MainBackground>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} tintColor={'#fcfcfc'} />
        }
      >
        <View style={styles.topView}>
          <View style={styles.view}>
            <View
              style={[
                styles.flex,
                { shadowColor: deviceStatus ? '#2ecc71' : colors.redButtonColor },
              ]}
            >
              <Text style={styles.glow}>Device Status : </Text>
              <Text
                style={[
                  styles.glow,
                  {
                    textShadowColor: deviceStatus ? '#2ecc71' : colors.redButtonColor,
                    color: deviceStatus ? '#2ecc71' : colors.redButtonColor,
                  },
                ]}
              >
                {deviceStatus ? `Active` : `Not Active`}
              </Text>
            </View>
          </View>
          <View style={{ width: '100%', marginBottom: '2%' }}>
            <AppText style={{ fontSize: 12, color: '#FFFFFF' }}>Event Name</AppText>
            <AppPicker
              items={data}
              onSelectItem={(item) => {
                setSelectedItem(item);
                auth.setItem(item);
                // storage.storeEventData(item)
              }}
              PickerItemComponent={UserPickerItem}
              placeholder={'-- Select --'}
              selectedItem={selectedItem}
            />
          </View>
          {itemLoading ? (
            <ActivityIndicatorNative size={'large'} color={'#fcfcfc'} />
          ) : (
            <>
              <View style={{ width: '100%' }}>
                {Card({
                  cardName: 'Total Issued Tickets',
                  per: cardData?.totalTicket?.totalPercentage,
                  title: 'Total',
                  subtitle: cardData?.totalTicket?.totalTicketsCount,
                  title2: 'Scanned',
                  subtitle2: cardData?.totalTicket?.totalScannedCount,
                })}
              </View>
              <View style={{ width: '100%' }}>
                {Card({
                  cardName: 'Scanned Status',
                  per: cardData?.myTicket?.myPercentage,
                  textLarge: true,
                  title: 'Total Scanned',
                  subtitle: cardData?.totalTicket?.totalScannedCount,
                  title2: 'Scanned by me',
                  subtitle2: cardData?.myTicket?.myScannedCount,
                })}
              </View>
            </>
          )}
        </View>
      </ScrollView>
    </MainBackground>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  topView: {
    padding: '5%',
    paddingTop: '10%',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  cardView: {
    flexGrow: 1,
    backgroundColor: colors.buttonBackground,
    borderRadius: 10,
    marginVertical: 10,
    paddingHorizontal: '3%',
    paddingVertical: '5%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardInnerView: {},
  cardInnerViewText: {
    flexGrow: 1,
    flex: 5,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tag: { fontSize: 16, color: '#FFFFFF', fontWeight: 'bold' },
  cardTitle: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 16,
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  cardTitleLarge: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 16,
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  cardSubTitleLarge: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 20,
    textTransform: 'capitalize',
  },
  cardSubTitle: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 20,
    textTransform: 'capitalize',
  },
  glow: {
    color: '#fcfcfc',
    fontSize: 12,
    lineHeight: 12,
    letterSpacing: 0.5,
  },
  view: {
    width: '100%',
    marginVertical: '2%',
    alignItems: 'flex-end',
  },
  flex: {
    flexDirection: 'row',
    borderColor: '#666666',
    borderWidth: 1,
    borderRadius: 5,
    padding: 5,
    width: '50%',
    justifyContent: 'center',

    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
});

export default HomeScreen;
