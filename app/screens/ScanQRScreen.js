import React, { useState, useRef, useEffect } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Platform, Button } from 'react-native';
import MainBackground from '../components/MainBackground';
import QRCodeScanner from 'react-native-qrcode-scanner';
import colors from '../config/colors';
import { RNCamera } from 'react-native-camera';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import AppText from '../components/Text';
import store from '../auth/storage';
import Geolocation from 'react-native-geolocation-service';
import client from '../api/client';
import * as Sentry from '@sentry/react-native';
import { useIsFocused } from '@react-navigation/native';
import useAuth from '../auth/useAuth';
let FlashtextOn = 'Flash mode On';
let FlashtextOff = 'Flash mode Off';

function ScanQRScreen({ navigation }) {
  const { logOut } = useAuth();
  const isFocused = useIsFocused();
  const [toggleState, setToggleState] = useState(false);
  const [flash, setflash] = useState(false);
  const refScanner = useRef(null);

  const onSuccess = async (value) => {
    if (isFocused) {
      try {
        var Data = await JSON.parse(value?.data);
        client.setBaseURL(`${Data.baseUrl}`);
        await store.storeBaseURLOnly(Data.baseUrl);
        await store.storeBaseURL(value.data);
        navigation.navigate('AddInformationScreen');
      } catch (error) {
        Sentry.captureException(error);
      }
    }
  };

  const flashHandle = () => {
    setflash(!flash);
  };

  const howToUse = () => {
    navigation.navigate('HowToScanScreen');
  };

  useEffect(() => {
    if (isFocused) {
      refScanner.current.reactivate();
    }
    return () => {};
  }, [isFocused]);

  useEffect(() => {
    if (Platform.OS === 'ios') {
      Geolocation.requestAuthorization('whenInUse');
    }
  }, []);

  return (
    <MainBackground>
      <View style={styles.mainContainer}>
        <TouchableOpacity
          onPress={flashHandle}
          style={{
            right: 0,
            top: 0,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Ionicons name={flash ? 'flash' : 'flash-off'} color={colors.white} size={24} />
          <AppText style={{ marginHorizontal: '1%' }}>{flash ? FlashtextOn : FlashtextOff}</AppText>
        </TouchableOpacity>
        <View style={styles.scanView}>
          <View
            style={{
              height: '80%',
              width: '100%',
            }}
          >
            <QRCodeScanner
              showMarker={true}
              markerStyle={{ borderColor: colors.white, borderStyle: 'dashed' }}
              ref={refScanner}
              reactivate={false}
              flashMode={
                flash ? RNCamera.Constants.FlashMode.torch : RNCamera.Constants.FlashMode.off
              }
              cameraStyle={{
                height: '100%',
                width: '100%',
              }}
              cameraContainerStyle={{
                height: '100%',
                width: '100%',
                alignSelf: 'center',
              }}
              onRead={onSuccess}
            />
          </View>
          <TouchableOpacity style={{ top: '3%' }} onPress={howToUse}>
            <Text style={styles.howToUse}>How to use</Text>
          </TouchableOpacity>
        </View>
      </View>
    </MainBackground>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  scanView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  howToUse: {
    fontSize: 18,
    color: colors.white,
    lineHeight: 18,
    fontStyle: 'normal',
    textDecorationLine: 'underline',
  },
  button: {
    borderRadius: 100,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    bottom: '2%',
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});

export default ScanQRScreen;
