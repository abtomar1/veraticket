import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  useWindowDimensions,
  ScrollView,
  ActivityIndicator as Activity,
} from 'react-native';
import MainBackground from '../components/MainBackground2';
import AppText from '../components/Text';
import Colors from '../config/colors';
import AppPicker from '../components/UserPicker';
import UserPickerItem from '../components/UserPickerItem';
import ActivityIndicator from '../components/ActivityIndicator';
import ImageBackground from '../components/ImageBackground2';
import AppButton from '../components/Button';
import listingsApi from '../api/listings';
import store from '../auth/storage';
import moment from 'moment';
import useAuth from '../auth/useAuth';
import * as Sentry from '@sentry/react-native';
import LottieView from 'lottie-react-native';
import RenderHtml, { defaultSystemFonts } from 'react-native-render-html';

let Data = [
  {
    id: 0,
    uri: 'https://images.unsplash.com/photo-1607326957431-29d25d2b386f',
    title: 'James Forman - Grand Toure in California',
  },
];
function AllEventsScreen({ navigation, route }) {
  const auth = useAuth();
  const { width } = useWindowDimensions();
  const [data, setData] = useState([]);
  const [selectedItem, setSelectedItem] = useState(auth.seletedItem);
  const [loading, setloading] = useState(true);
  const [imageloading, setimageloading] = useState(false);

  const buttonPressed = () => {
    if (selectedItem) {
      navigation.navigate('EventScannerScreen', { data: selectedItem });
    }
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setSelectedItem(auth.seletedItem);
      getEventListRefresh();
    });
    return unsubscribe;
  }, [navigation, auth.seletedItem]);

  const getbaseURL = async () => {
    let data = await store.getBaseURL();
    return data;
  };

  const getEventListRefresh = async () => {
    let baseURLData = JSON.parse(JSON.parse(await getbaseURL()));
    try {
      const authToken = JSON.parse(await store.getToken());
      const result = await listingsApi.getEventData(baseURLData.baseUrl, authToken?.id);

      if (result?.ok) {
        let eventList = [];
        result.data?.data?.rows.map((val) => {
          let uri = val?.eventImage
            ? result.data?.metaDetails?.domainUrl + val?.eventImage
            : 'https://i.picsum.photos/id/987/536/354.jpg?hmac=qMAJC_SCI2Tp7gCUYRyTsWE2rVFqGajrxQKTFCkg-FI';
          eventList.push({
            id: val.id,
            userId: val.userId,
            title: val.name,
            about: val.about,
            venue: val.venue,
            uri: uri,
            startDate: val.startDate,
            venue: val.venue,
            about: val.about,
          });
        });
        setData(eventList);
      } else {
        Sentry.captureException(result.data?.message);
      }
    } catch (error) {
      Sentry.captureException(error);
      console.log('Error ', error);
    }
  };

  const getEventList = async () => {
    let baseURLData = JSON.parse(JSON.parse(await getbaseURL()));
    try {
      const authToken = JSON.parse(await store.getToken());
      const result = await listingsApi.getEventData(baseURLData.baseUrl, authToken?.id);

      if (result?.ok) {
        let eventList = [];
        result.data?.data?.rows.map((val) => {
          let uri = val?.eventImage
            ? result.data?.metaDetails?.domainUrl + val?.eventImage
            : 'https://i.picsum.photos/id/987/536/354.jpg?hmac=qMAJC_SCI2Tp7gCUYRyTsWE2rVFqGajrxQKTFCkg-FI';
          eventList.push({
            id: val.id,
            userId: val.userId,
            title: val.name,
            about: val.about,
            venue: val.venue,
            uri: uri,
            startDate: val.startDate,
            venue: val.venue,
            about: val.about,
          });
        });
        setData(eventList);
        setSelectedItem(auth.seletedItem);
        setloading(false);
      } else {
        Sentry.captureException(result.data?.message);
      }
    } catch (error) {
      Sentry.captureException(error);
      console.log('Error ', error);
    }
  };
  const renderHtml = (item) => {
    let result = item.replace('<font', '<span');
    let result1 = result.replace('</font>', '</span>');
    return (
      <RenderHtml
        contentWidth={width}
        tagsStyles={{
          p: styles.rowRightViewInner,
          div: styles.rowRightViewInner,
          span: styles.rowRightViewInner,
          a: styles.rowRightViewInner,
        }}
        source={{
          html: `<span>${result1}</span>`,
        }}
      />
    );
  };

  useEffect(() => {
    getEventList(Data);
  }, []);

  return loading ? (
    <ActivityIndicator visible={loading} />
  ) : data.length == 0 ? (
    <MainBackground>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: '40%',
        }}
      >
        <LottieView
          style={{ width: 150, height: 150 }}
          source={require('../assets/animations/empty.json')}
          autoPlay
        />
        <AppText style={{ fontSize: 18, color: '#FFFFFF', fontWeight: 'bold' }}>
          Currenly you have no event
        </AppText>
      </View>
    </MainBackground>
  ) : (
    <ImageBackground
      onLoadStarti={(value) => {
        setimageloading(value);
      }}
      onLoadEndi={(value) => {
        setimageloading(value);
      }}
      Data={selectedItem.uri}
      route={route}
      title={'Scan'}
    >
      <View style={{ marginTop: 20, height: '95%' }}>
        <View style={{ width: '90%', left: '5%' }}>
          <AppPicker
            items={data}
            onSelectItem={(item) => {
              setSelectedItem(item);
              auth.setItem(item);
            }}
            PickerItemComponent={UserPickerItem}
            placeholder={'-- Select --'}
            selectedItem={selectedItem}
          />
        </View>

        {imageloading ? (
          <View style={styles.innerContainer}>
            <Activity size="large" color={Colors.white} />
          </View>
        ) : (
          <ScrollView style={{ marginVertical: '5%' }}>
            <View style={styles.innerContainer}>
              <AppText style={styles.eventTitle}>{selectedItem.title}</AppText>
              <View style={styles.rowView}>
                <AppText style={styles.rowLeftView}>{`Start Date `}</AppText>
                <AppText style={styles.middledot}>{`:`}</AppText>
                <View style={styles.rowRightView}>
                  <AppText style={styles.rowRightViewInner}>
                    {`${moment(selectedItem.startDate).format('LLLL')}`}
                  </AppText>
                </View>
              </View>
              <View style={styles.rowView}>
                <AppText style={styles.rowLeftView}>{`Venue `}</AppText>
                <AppText style={styles.middledot}>{`:`}</AppText>
                <View style={styles.rowRightView}>
                  <AppText style={styles.rowRightViewInner}>{`${selectedItem.venue}`}</AppText>
                </View>
              </View>
              <View style={styles.rowView}>
                <AppText style={styles.rowLeftView}>{'About '}</AppText>
                <AppText style={styles.middledot}>{`:`}</AppText>
                <View style={styles.rowRightView}>{renderHtml(`${selectedItem.about}`)}</View>
              </View>
            </View>
          </ScrollView>
        )}
        <View style={{ alignSelf: 'center' }}>
          <AppButton
            onPress={buttonPressed}
            title={'Scan Now'}
            style={{ fontWeight: 'bold', color: '#fcfcfc', fontSize: 18 }}
          />
        </View>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    marginTop: '10%',
  },

  rowView: {
    flexDirection: 'row',
    width: '90%',
    marginTop: '5%',
  },
  middledot: {
    flex: 0.3,
    fontSize: 14,
    color: '#fcfcfc',
    fontWeight: '600',
    lineHeight: 16,
  },
  rowRightView: { flex: 9 },
  rowRightViewInner: {
    color: '#fcfcfc',
    fontWeight: '600',
    lineHeight: 16,
    fontSize: 14,
    textAlign: 'left',
  },
  rowLeftView: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 16,
    textAlign: 'left',
    flex: 3,
  },
  tinyLogo: {
    width: 200,
    height: 200,
  },
  about: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 16,
    textAlign: 'left',
    width: '90%',
    marginVertical: '3%',
  },
  innerContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    paddingTop: '10%',
  },
  eventTitle: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 26,
    lineHeight: 26,
    textAlign: 'left',
    width: '90%',
    marginVertical: '1%',
  },
});

export default AllEventsScreen;
