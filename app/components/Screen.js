import React from 'react';
import { StyleSheet, SafeAreaView, View } from 'react-native';
import colors from '../config/colors';
import LinearGradient from 'react-native-linear-gradient';

function Screen({ children, style }) {
  return (
    <SafeAreaView style={[styles.screen, style]}>
      <LinearGradient
        colors={[colors.redButtonColor, colors.black]}
        start={{ x: 1, y: 2 }}
        end={{ x: 1, y: 0.5 }}
        style={styles.view}
      >
        {children}
      </LinearGradient>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  screen: {
    paddingTop: 30,
    flex: 1,
    backgroundColor: colors.black,
  },
  view: {
    flex: 1,
  },
});

export default Screen;
