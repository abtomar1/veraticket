import React, { useState } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import colors from '../config/colors';
import { ErrorMessage, Form, FormField, SubmitButton } from './forms';
import * as Yup from 'yup';
import CustomImagePicker from './ImagePicker';

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .min(4, 'Minimum 4 characters needed.')
    .required('Name cannot be empty')
    .matches(/^[a-zA-Z\s]*$/, 'Special characters and numbers not allowed')
    .label('Name'),
});

function AddInfo({ navigation, onSubmit, info, buttonLoading = false }) {
  const [imageUrl, setImageUrl] = useState({ url: null });

  const handleSubmit = (r, { setErrors }) => {
    let name = r.name.trim();
    if (name == '') {
      setErrors({ name: 'Space and special characters are not allowed.' });
      return;
    }
    onSubmit(r);
  };
  return (
    <View style={styles.mainContainer}>
      <View>
        <Form
          initialValues={{ name: info?.name ? info.name : '' }}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
        >
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            name="name"
            topTitle="Name"
            placeholder="e.g. John Doe"
          />
          <View style={styles.buttonBox}>
            <SubmitButton
              title="Update"
              style={styles.submitButtonText}
              buttonLoading={buttonLoading}
            />
          </View>
        </Form>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
  },
  buttonBox: {
    marginTop: '5%',
  },
  submitButtonText: {
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 19,
    textAlign: 'center',
    color: colors.white,
  },
});

export default AddInfo;
