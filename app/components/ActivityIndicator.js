import React from 'react';
import LottieView from 'lottie-react-native';
import { View, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../config/colors';

function ActivityIndicator({ visible = false }) {
  if (!visible) return null;

  return (
    <LinearGradient
      colors={[colors.redButtonColor, colors.black]}
      start={{ x: 1, y: 2 }}
      end={{ x: 1, y: 0.5 }}
      style={{
        flex: 1,
        borderRadius: 5,
      }}
    >
      <View style={styles.overlay}>
        <LottieView autoPlay loop source={require('../assets/animations/loader.json')} />
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  overlay: {
    position: 'absolute',
    backgroundColor: 'transparent',
    height: '100%',
    opacity: 0.8,
    width: '100%',
    zIndex: 1,
  },
});

export default ActivityIndicator;
