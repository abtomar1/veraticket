import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';

function AppTextInput({ icon, width = '100%', ...otherProps }) {
  return (
    <View style={[styles.container, { width }]}>
      <TextInput placeholderTextColor="#fcfcfc" style={styles.input} {...otherProps} />
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    width: '85%',
    padding: 13,
    borderWidth: 0,
    flex: 6,
    fontSize: 12,
    color: '#fcfcfc',
  },
  container: {
    backgroundColor: '#222222',
    flexDirection: 'row',
    marginVertical: 10,
    borderRadius: 12,
    height: 56,
    alignSelf: 'center',
    width: 315,
    height: 50,
    borderRadius: 100,
  },
  icon: {
    padding: 13,
    paddingRight: 0,
    marginRight: 0,
    marginTop: -1,
    alignSelf: 'center',
  },
});

export default AppTextInput;
