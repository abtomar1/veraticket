import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, View, StatusBar } from 'react-native';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import colors from '../config/colors';
import { useNavigation } from '@react-navigation/native';
import AppText from './Text';
import Colors from '../config/colors';

function HeaderPage({
  title,
  backgroundColor = false,
  navigation2,
  route,
  previousScreen,
  setfilterModal,
  filterModal,
}) {
  let navigation = useNavigation();
  const goBack = () => {
    navigation.goBack();
  };

  return (
    <React.Fragment>
      <View
        style={[styles.head, { backgroundColor: backgroundColor ? backgroundColor : colors.black }]}
      >
        <View style={styles.wrapper}>
          <TouchableOpacity onPress={goBack}>
            <Ionicons name="ios-chevron-back-outline" size={30} color={colors.white} />
          </TouchableOpacity>
          <View style={styles.view1}>
            <AppText style={styles.text2}>{title}</AppText>
          </View>
        </View>
      </View>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.buttonBackground,
  },
  view1: { justifyContent: 'center', alignItems: 'center', flex: 1, paddingRight: '10%' },
  text2: { fontSize: 20, fontWeight: 'bold' },
  head: {
    flex: 1,
    width: '100%',
    top: StatusBar.currentHeight,
    left: 0,
    position: 'absolute',
    height: 50,
  },
  left: {
    flex: 1,
  },
  rightIconContaner: {
    justifyContent: 'center',
    zIndex: 10,
  },
  rightIcon: {
    padding: 8,
    marginLeft: 8,
    paddingLeft: 10,
  },
  right: {
    flex: 1,
    justifyContent: 'center',
  },
  serachContainer: {
    backgroundColor: colors.white,
    paddingHorizontal: 10,
    borderRadius: 4,
    height: 40,
  },
  wrapper: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    paddingHorizontal: '5%',
  },
  searchIcon: {
    fontSize: 20,
    paddingTop: 5,
    color: colors.white,
  },
  icon: {
    color: colors.white,
  },
  input: {
    fontSize: 14,
    marginTop: 3,
  },
  text: {
    color: colors.white,
  },
});

export default HeaderPage;
