import React, { useState, useEffect } from 'react';

import { StyleSheet, Text, View, TouchableOpacity, Button, Image } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import colors from '../config/colors';

function CustomImagePicker({ children, navigation, setImageUrl }) {
  const [state, setState] = useState({
    resourcePath: {},
  });

  const launchImageLibrarys = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
        alert(response.customButton);
      } else {
        const source = { uri: response.uri };

        setState({
          filePath: response,
          fileData: response.data,
          fileUri: response.assets[0].uri,
        });
        setImageUrl(response.assets[0].uri);
      }
    });
  };

  const renderFileUri = () => {
    if (state.fileUri) {
      return <Image source={{ uri: state.fileUri }} style={styles.image} />;
    } else {
      return <Image source={require('../assets/profile.png')} style={styles.image} />;
    }
  };
  return (
    <View style={styles.container}>
      {renderFileUri()}
      <TouchableOpacity style={styles.cameraView} onPress={launchImageLibrarys}>
        <MaterialCommunityIcons
          name={'camera'}
          color={colors.white}
          size={20}
          style={{ left: 1 }}
        />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  cameraView: {
    height: 40,
    width: 40,
    backgroundColor: '#313131',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: '30%',
    bottom: 0,
  },
  image: {
    height: 120,
    width: 120,
    borderRadius: 120 / 2,
    backgroundColor: '#fff',
  },
  button: {
    width: 250,
    height: 60,
    backgroundColor: '#3740ff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    marginBottom: 12,
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 15,
    color: '#fff',
  },
});

export default CustomImagePicker;
