import React, { useState } from 'react';
import { View, StyleSheet, Text, StatusBar, ImageBackground } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Header from './Header';

const ImageBackgroundS = ({ children, navigation, Data, route, title }) => {
  const STYLES = ['default', 'dark-content', 'light-content'];
  const TRANSITIONS = ['fade', 'slide', 'none'];
  const [statusBarStyle, setStatusBarStyle] = useState('light-content');
  const [statusBarTransition, setStatusBarTransition] = useState(TRANSITIONS[0]);
  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor="#000000"
        barStyle={statusBarStyle}
        showHideTransition={statusBarTransition}
      />

      <ImageBackground source={{ uri: Data.uri }} style={styles.backgroundImage}>
        <LinearGradient
          colors={['#000', 'transparent']}
          start={{ x: 1, y: 0.75 }}
          end={{ x: 1, y: 0.2 }}
          style={{ backgroundColor: 'transparent', flex: 1 }}
        >
          <View style={styles.headerView}>
            <Header
              title={title}
              backgroundColor={'transparent'}
              navigation={navigation}
              route={route}
            />
          </View>
          {children}
        </LinearGradient>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
  },
  headerView: {
    flex: 1,
    zIndex: 1,
    top: 50,
    position: 'absolute',
    height: 50,
    width: '100%',
  },
});

export default ImageBackgroundS;
