import React from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';
import colors from '../config/colors';
import HeaderPage from './Header';
import { useNavigation } from '@react-navigation/native';

function Layout({
  children,
  filterModal,
  setfilterModal,
  containerStyle,
  contentStyle,
  headerTitle,
  route,
  previousScreen,
}) {
  let navigation = useNavigation();
  const goBack = () => {
    navigation.goBack();
  };
  return (
    <View style={[styles.container, containerStyle]}>
      <HeaderPage
        title={headerTitle}
        filterModal={filterModal}
        setfilterModal={setfilterModal}
        navigation={navigation}
        route={route}
        previousScreen={previousScreen}
      />
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 1,
  },
});

export default Layout;
