import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';

import colors from '../config/colors';
import AppText from './Text';

function UserPickerItem({ item, onPress }) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress} style={styles.wrapper}>
        <AppText style={styles.label}>{item.label}</AppText>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.buttonBackground,
    marginTop: 8,
    padding: 10,
    borderWidth: 0.5,
    borderColor: colors.black,
    marginLeft: 20,
    marginRight: 20,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  label: {
    alignSelf: 'center',
    marginTop: 5,
  },
  icon: {
    alignSelf: 'center',
    marginLeft: 10,
    marginTop: 4,
  },
});

export default UserPickerItem;
