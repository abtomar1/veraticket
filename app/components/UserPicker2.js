import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Modal,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';

import AppText from './Text';
import colors from '../config/colors';
import UserPickerItem from './UserPickerItem';
import Screen from './Screen';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import Colors from '../config/colors';
import storage from '../auth/storage';

function AppPicker({
  icon,
  items,
  numberOfColumns = 1,
  onSelectItem,
  PickerItemComponent = UserPickerItem,
  placeholder,
  selectedItem,
  width = '100%',
  clearClose,
  clearIcon,
}) {
  const [modalVisible, setModalVisible] = useState(false);

  const ItemCard = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          setModalVisible(false);
          onSelectItem(item);
          storage.storeEventData(item);
        }}
        style={{
          flexDirection: 'row',
          padding: 10,
          backgroundColor: selectedItem.id == item.id ? colors.redButtonColor : null,
          borderRadius: 10,
          marginHorizontal: '2%',
          height: 70,
        }}
      >
        <View style={styles.view}>
          <AppText style={styles.text3}>{item.title}</AppText>
          <AppText style={styles.text2}>Organizer Name : {item?.organizerName}</AppText>
        </View>
        <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center' }}>
          <MaterialIcons size={14} color={Colors.white} name={'arrow-forward-ios'} />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <>
      <TouchableWithoutFeedback onPress={() => setModalVisible(true)}>
        <View style={[styles.container, { width }]}>
          {selectedItem ? (
            <AppText style={styles.text}>{selectedItem.title}</AppText>
          ) : (
            <AppText style={styles.text}>{placeholder}</AppText>
          )}

          <MaterialCommunityIcons name="chevron-down" size={20} color={colors.white} />
        </View>
      </TouchableWithoutFeedback>
      <Modal visible={modalVisible} animationType="slide">
        <Screen>
          <View style={{ paddingHorizontal: 15, paddingTop: '4%' }}>
            <View style={{ alignSelf: 'flex-end' }}>
              <TouchableOpacity
                onPress={() => {
                  setModalVisible(false);
                  clearClose ? onSelectItem(null) : null;
                }}
              >
                <Entypo size={30} color={Colors.white} name={'circle-with-cross'} />
              </TouchableOpacity>
            </View>
          </View>
          <ScrollView contentContainerStyle={{}} showsVerticalScrollIndicator={false}>
            <View style={styles.mainContainer}>
              <View style={{}}>
                {items.map((item, index) => {
                  return <ItemCard item={item} key={index} />;
                })}
              </View>
            </View>
          </ScrollView>
        </Screen>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    marginTop: '8%',
  },
  tinyLogo: {
    width: 200,
    height: 200,
  },
  view: { flex: 4, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 5 },
  container: {
    backgroundColor: colors.buttonBackground,
    marginVertical: 5,
    borderRadius: 100,
    flexDirection: 'row',
    height: 50,
    width: 315,
    alignSelf: 'center',
    paddingLeft: 13,
    paddingRight: 13,
    alignItems: 'center',
  },
  text2: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 12,
    lineHeight: 19,
    alignSelf: 'flex-start',
  },
  text3: {
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 19,
    alignSelf: 'flex-start',
  },
  icon: {
    marginRight: 10,
  },
  placeholder: {
    fontSize: 14,
    color: '#333333',
    flex: 1,
  },
  text: {
    flex: 1,
    color: colors.white,
    fontSize: 14,
    fontWeight: 'bold',
  },
});

export default AppPicker;
