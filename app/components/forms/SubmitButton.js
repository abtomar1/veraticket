import React from 'react';
import { useFormikContext } from 'formik';

import AppButton from '../Button';

function SubmitButton({ title, style, buttonLoading = false }) {
  const { handleSubmit } = useFormikContext();

  return (
    <AppButton title={title} onPress={handleSubmit} style={style} buttonLoading={buttonLoading} />
  );
}

export default SubmitButton;
