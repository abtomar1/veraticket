import React from 'react';
import { useFormikContext } from 'formik';
import { View, Text } from 'react-native';
import TextInput from '../TextInput';
import ErrorMessage from './ErrorMessage';

function AppFormField({
  name,
  topTitle = false,
  defalut = false,
  onChanges = false,
  width,
  ...otherProps
}) {
  const { setFieldTouched, setFieldValue, errors, touched, values } = useFormikContext();

  return (
    <React.Fragment>
      <View>
        {topTitle ? <Text style={{ color: '#FFFFFF', fontWeight: 'bold' }}>{topTitle}</Text> : null}
        <TextInput
          onBlur={() => setFieldTouched(name)}
          onChangeText={(text) => {
            setFieldValue(name, text), onChanges ? onChanges(text) : null;
          }}
          value={values[name]}
          width={width}
          {...otherProps}
        />
        <View style={{ width: '80%', marginBottom: 5 }}>
          <ErrorMessage error={errors[name]} visible={touched[name]} />
        </View>
      </View>
    </React.Fragment>
  );
}

export default AppFormField;
