import React from 'react';
import { useFormikContext } from 'formik';

import Picker from '../UserPicker';

function UserFormPicker({
  items,
  name,
  numberOfColumns,
  PickerItemComponent,
  placeholder,
  width,
  clearClose,
  seletedVendor = false,
  clearIcon = false,
  onChange = false,
}) {
  return (
    <>
      <Picker
        items={items}
        numberOfColumns={numberOfColumns}
        onSelectItem={(item) => {
          onChange ? onChange(item) : null;
        }}
        PickerItemComponent={PickerItemComponent}
        placeholder={placeholder}
        selectedItem={items[0]}
        width={width}
        clearClose={clearClose}
        clearIcon={clearIcon}
      />
    </>
  );
}

export default UserFormPicker;
