import React from 'react';
import { StyleSheet } from 'react-native';

import AppText from '../Text';
import defaultStyles from '../../config/styles';

function SuccessMessage({ success, visible }) {
  if (!visible || !success) return null;

  return <AppText style={[styles.success, defaultStyles.fontFamily]}>{success}</AppText>;
}

const styles = StyleSheet.create({
  success: {
    color: 'green',
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 20,
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 16,
  },
});

export default SuccessMessage;
