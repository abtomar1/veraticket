import React from 'react';

import { Alert, Platform, PermissionsAndroid } from 'react-native';

import RNFetchBlob from 'rn-fetch-blob';

export default Dashboard = () => {
  const downloadFile = (fileUrl) => {
    let date = new Date();

    let FILE_URL = fileUrl;

    let file_ext = getFileExtention(FILE_URL);

    file_ext = '.' + file_ext[0];

    const { config, fs } = RNFetchBlob;
    let RootDir = fs.dirs.DownloadDir;
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        path: RootDir + '/VeraticketScanner' + file_ext,
        description: 'downloading file...',
        notification: true,
        useDownloadManager: true,
      },
    };
    return config(options)
      .fetch('GET', FILE_URL)
      .then((res) => {
        alert(`File downloaded successfully at ${res?.data}`);
        return JSON.stringify(res);
      });
  };

  const getFileExtention = (fileUrl) => {
    return /[.]/.exec(fileUrl) ? /[^.]+$/.exec(fileUrl) : undefined;
  };

  const checkPermission = async (fileUrl) => {
    if (Platform.OS === 'ios') {
      return downloadFile(fileUrl);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message: 'Application needs access to your storage to download File',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          return downloadFile(fileUrl);
        } else {
          Alert.alert('Error', 'Storage Permission Not Granted');
          return { data: 'Storage Permission Not Granted' };
        }
      } catch (err) {
        console.log('++++' + err);
        return { data: 'Something went wrong!' };
      }
    }
  };

  return { checkPermission };
};
