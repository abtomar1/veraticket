import React, { useState } from 'react';
import { StatusBar, StyleSheet, SafeAreaView } from 'react-native';
import colors from '../config/colors';
import LinearGradient from 'react-native-linear-gradient';

function MainBackground({ children, navigation }) {
  const STYLES = ['default', 'dark-content', 'light-content'];
  const TRANSITIONS = ['fade', 'slide', 'none'];
  const [statusBarStyle, setStatusBarStyle] = useState(STYLES[2]);
  const [statusBarTransition, setStatusBarTransition] = useState(TRANSITIONS[0]);
  return (
    <LinearGradient
      colors={[colors.redButtonColor, colors.black]}
      start={{ x: 1, y: 2 }}
      end={{ x: 1, y: 0.5 }}
      style={styles.linearGradient}
    >
      <StatusBar
        backgroundColor="#000000"
        barStyle={statusBarStyle}
        showHideTransition={statusBarTransition}
      />
      <SafeAreaView>{children}</SafeAreaView>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  linearGradient: {
    flex: 1,
    borderRadius: 5,
  },
});

export default MainBackground;
