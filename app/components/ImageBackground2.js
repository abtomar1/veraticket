import React, { useState } from 'react';
import { View, StyleSheet, Text, StatusBar, ImageBackground, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Header from './Header';

const ImageBackgroundS = ({
  children,
  navigation,
  Data,
  route,
  title,
  onLoadStarti,
  onLoadEndi,
}) => {
  const STYLES = ['default', 'dark-content', 'light-content'];
  const TRANSITIONS = ['fade', 'slide', 'none'];
  const [statusBarStyle, setStatusBarStyle] = useState('light-content');
  const [statusBarTransition, setStatusBarTransition] = useState(TRANSITIONS[0]);
  return (
    <View style={styles.container}>
      <ImageBackground
        resizeMode={'cover'}
        source={{ uri: Data }}
        style={styles.backgroundImage}
        onLoadStart={() => onLoadStarti(true)}
        onLoadEnd={() => onLoadEndi(false)}
      >
        <LinearGradient
          colors={['#000', 'transparent']}
          start={{ x: 1, y: 0.7 }}
          end={{ x: 1, y: 0 }}
          style={{ backgroundColor: 'transparent', flex: 1, paddingVertical: '10%' }}
        >
          {children}
        </LinearGradient>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'contain',
  },
  headerView: {
    flex: 1,
    zIndex: 1,
    top: 50,
    position: 'absolute',
    height: 50,
    width: '100%',
  },
});

export default ImageBackgroundS;
