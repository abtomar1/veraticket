import React, { useEffect, useState } from 'react';
import { Modal, StyleSheet, View, TouchableOpacity, TextInput, Text } from 'react-native';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import AppText from './Text';
import colors from '../config/colors';
import client from '../api/client';
import store from '../auth/storage';

const EnvironmentSwitcher = ({ modalVisible, setShowEnvironmentSwitcher }) => {
  const [customLink, setCustomLink] = useState(client.getBaseURL());
  const [selectedEnv, setSelectedEnv] = useState(null);
  const handleConfirm = async () => {
    if (customLink !== '') {
      client.setBaseURL(customLink);
      await store.storeBaseURLOnly(customLink);
      setShowEnvironmentSwitcher(!modalVisible);
    } else {
      alert(`Custom link is not correct`);
    }
  };

  const getSavedEnv = async () => {
    console.log(`omg i am now at ${client.getBaseURL()}`);
  };

  useEffect(() => {
    getSavedEnv();
  }, []);

  const renderCloseView = () => {
    return (
      <TouchableOpacity
        style={{ position: 'absolute', right: 10, top: 10 }}
        hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
        onPress={async () => {
          setShowEnvironmentSwitcher(!modalVisible);
        }}
      >
        <Entypo name="circle-with-cross" size={27} color="#000" />
      </TouchableOpacity>
    );
  };
  const renderTextInputView = () => {
    return (
      <View style={{}}>
        <View
          style={{
            backgroundColor: colors.white,
            paddingTop: 10,
          }}
        >
          <TextInput
            autoCapitalize="none"
            autoCorrect={false}
            value={customLink}
            onChangeText={(queryText) => {
              setCustomLink(queryText);
            }}
            placeholder={'Link*'}
            style={styles.inputStyle}
          />
        </View>
      </View>
    );
  };

  const renderConfirmButton = () => {
    return (
      <TouchableOpacity style={styles.button} onPress={handleConfirm}>
        <Text style={styles.textButton}>Confirm</Text>
      </TouchableOpacity>
    );
  };

  const renderEnvSelectionView = (index, environment) => {
    return (
      <TouchableOpacity
        style={{
          left: 30,
          top: 10,
          flexDirection: 'row',
          alignItems: 'center',
          marginVertical: 10,
        }}
        onPress={() => setSelectedEnv(index)}
      >
        <AppText
          style={[styles.envTitle, selectedEnv === index ? { color: colors.primaryLight } : {}]}
        >
          {environment}
        </AppText>
      </TouchableOpacity>
    );
  };

  const renderEnvList = () => {
    return (
      <View style={{ flex: 1, marginTop: 20 }}>
        {renderEnvSelectionView('custom', 'Dev Custom')}
        {renderTextInputView()}
      </View>
    );
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setShowEnvironmentSwitcher(!modalVisible);
      }}
    >
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          padding: 20,
          backgroundColor: 'rgba(0,0,0,0.7)',
        }}
      >
        <View style={[styles.container, { height: selectedEnv === 4 ? 360 : 290 }]}>
          <Text style={styles.headerText}>Environment URL</Text>
          {renderEnvList()}
          {renderConfirmButton()}
          {renderCloseView()}
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    alignSelf: 'center',
    marginBottom: 50,
    height: 350,
    width: '100%',
    borderRadius: 10,
    padding: 10,
  },
  textButton: {
    color: '#fcfcfc',
    fontSize: 14,
  },
  centeredView: {},
  heading: {
    fontSize: 18,
    color: '#333333',
    // fontFamily: defaultStyles.boldFont,
  },
  error: {
    fontSize: 14,
    color: 'red',
    bottom: '4%',
    paddingLeft: 5,
  },
  inputStyle: {
    backgroundColor: '#F2F2F2',
    marginVertical: 5,
    borderRadius: 12,
    height: 56,
    alignSelf: 'center',
    paddingHorizontal: 20,
    fontSize: 18,
    width: '80%',
  },
  containerButton: {
    flex: 1,

    paddingVertical: '2%',
    flexDirection: 'column',

    backgroundColor: '#fcfcfc',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  button: {
    backgroundColor: colors.buttonBackground,
    width: '50%',
    borderRadius: 5,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  envTitle: {
    // fontFamily: defaultStyles.mediumFont,
    fontSize: 16,
    color: '#333',
    marginLeft: 5,
  },
  headerText: {
    color: colors.black,
    fontSize: 16,
    alignSelf: 'center',
    // fontFamily: defaultStyles.boldFont,
    marginTop: 5,
  },
});

export default EnvironmentSwitcher;
