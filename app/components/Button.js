import React from 'react';
import { StyleSheet, Text, TouchableOpacity, ActivityIndicator, Keyboard } from 'react-native';

import colors from '../config/colors';

function AppButton({
  buttonLoading = false,
  title,
  onPress,
  color = false,
  style = false,
  textColor = 'white',
  fontSize = 16,
}) {
  return (
    <TouchableOpacity
      style={[styles.button, { backgroundColor: color ? color : colors.redButtonColor }]}
      onPress={() => {
        onPress(), Keyboard.dismiss();
      }}
    >
      {buttonLoading ? (
        <ActivityIndicator size={'small'} color={'#fcfcfc'} />
      ) : (
        <Text style={[styles.text, { color: textColor }]}>{title}</Text>
      )}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    width: 295,
    backgroundColor: colors.redButtonColor,
    borderRadius: 100,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: colors.white,
    textTransform: 'none',
    lineHeight: 19,
    textAlign: 'center',
    fontWeight: 'bold',
    fontStyle: 'normal',
  },
});

export default AppButton;
