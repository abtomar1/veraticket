const setBaseURL = (baseURL) => {
  let settings = {
    test: {
      apiUrl: 'https://e925-2405-201-300a-e824-15bf-4d30-1cbf-a718.ngrok.io',
    },
    prod: {
      apiUrl: 'https://XYZ.com',
    },
    baseURL: {
      apiUrl: 'https://dev.veraticket.com',
    },
  };
  settings.baseURL.apiUrl = baseURL;
};

const getCurrentSettings = (baseURL) => {
  let settings = {
    test: {
      apiUrl: 'https://e925-2405-201-300a-e824-15bf-4d30-1cbf-a718.ngrok.io',
    },
    prod: {
      apiUrl: 'https://XYZ.com',
    },
    baseURL: {
      apiUrl: baseURL,
    },
  };
  return settings.baseURL;
};

export default { getCurrentSettings, setBaseURL };
