export default {
  black: '#000000',
  textColor: '#ffffff',
  redButtonColor: '#FF2500',
  white: '#ffffff',
  buttonBackground: '#222222',
};
