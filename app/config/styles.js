import colors from './colors';

export default {
  boldFont: 'Gilroy-Bold',
  text: {
    color: colors.textColor,
    fontSize: 12,
    fontWeight: '500',
  },
  fontFamily: {
    fontFamily: 'UberMove-Regular',
  },
};
