import { useContext } from 'react';
import jwtDecode from 'jwt-decode';
import * as Sentry from '@sentry/react-native';
import AuthContext from './context';
import Itemcontext from './itemcontext';
import authStorage from './storage';
import NetworkUtils from './networkUtills';
import listings from '../api/listings';

export default useAuth = () => {
  const { user, setUser } = useContext(AuthContext);
  const { seletedItem, setseletedItem } = useContext(Itemcontext);

  const logIn = async (loginData) => {
    await authStorage.storeToken(loginData);
    await authStorage.storeLogin(true);
    const user = jwtDecode(loginData.token);
    let Data = { ...user, login: true };
    await authStorage.storeUserDetails(Data);
    setUser(Data);
  };

  const storeUser = async (loginData) => {
    const user = jwtDecode(loginData?.token);
    const list = {
      id: loginData?.organizerId,
      user: loginData,
      title: loginData?.name,
      organizerName: user?.organizerName,
    };
    const done = await authStorage.storeUsersList(list);
    if (done) {
      logIn(loginData);
    }
  };

  const restoreUserById = async (loginData) => {
    const user = await authStorage.getUserbyId(loginData?.user?.token);
    await authStorage.storeLogin(true);
    logIn(loginData?.user);
  };

  const deleteUserById = async (id) => {
    const user = await authStorage.deleteUsersList(id);
    if (user) {
      logOut();
      deLinkUser();
    } else {
      softlogOut();
    }
  };

  const deLinkUser = async () => {
    const isConnected = await NetworkUtils.isNetworkAvailable();
    if (isConnected) {
      try {
        const result = await listings.deLink();
        if (result?.ok) {
          return;
        }
      } catch (error) {
        Sentry.captureException(error);
        console.log('Error ', error);
      }
    } else {
      setTimeout(() => {
        deLinkUser();
      }, 10000);
    }
  };
  const restoreUser = async () => {
    const user = await authStorage.getUser();
    const login = await authStorage.getLogin();
    if (login) {
      let Data = { ...user, login: login };
      if (user) setUser(Data);
    } else {
      if (user) setUser(user);
    }
  };
  const restoreLoginUser = async () => {
    const user = await authStorage.getUser();
    await authStorage.storeLogin(true);
    let Data = { ...user, login: true };
    if (user) setUser(Data);
  };

  const setItem = async (item) => {
    setseletedItem(await item);
  };

  const softlogOut = async () => {
    let data = { ...user, login: false };
    await authStorage.storeLogin(false);
    setUser(data);
  };

  const logOut = async () => {
    setUser(null);
    await authStorage.storeLogin(null);
    authStorage.removeToken();
  };

  return {
    user,
    logIn,
    logOut,
    setItem,
    deleteUserById,
    restoreUserById,
    seletedItem,
    storeUser,
    softlogOut,
    restoreUser,
    restoreLoginUser,
  };
};
