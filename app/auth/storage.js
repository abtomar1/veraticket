import AsyncStorage from '@react-native-async-storage/async-storage';
import jwtDecode from 'jwt-decode';
import settings from '../config/settings';
var _ = require('lodash');
const tokenKey = 'tokenKey';
const EventData = 'EventData';
const baseURL = 'baseURL';
const baseURLOnly = 'baseURLOnly';
const login = 'login';
const userDetails = 'userDetails';
const storeUsersListKey = 'storeUsersListKey';

const storeToken = async (data) => {
  try {
    await AsyncStorage.setItem(tokenKey, JSON.stringify(data));
  } catch (error) {
    console.log('Error storing the LoginData', error);
  }
};

const storeLogin = async (data) => {
  try {
    await AsyncStorage.setItem(login, JSON.stringify(data));
  } catch (error) {
    console.log('Error storing the storeLogin', error);
  }
};

const getUsersList = async () => {
  try {
    let data = JSON.parse(await AsyncStorage.getItem(storeUsersListKey));
    return data;
  } catch (error) {
    console.log('Error storing the storeLogin', error);
  }
};

const storeUsersList = async (data) => {
  try {
    let list = JSON.parse(await AsyncStorage.getItem(storeUsersListKey));

    if (!list && list?.length !== 0) {
      const users = [data];
      await AsyncStorage.setItem(storeUsersListKey, JSON.stringify(users));
      return true;
    }
    let found = _.find(list, function (i) {
      return i?.id == data?.id;
    });
    if (found) {
      var filtered = list.filter(function (el) {
        return el.id != found?.id;
      });
      var newData = [...filtered, data];
      await AsyncStorage.setItem(storeUsersListKey, JSON.stringify(newData));
      return true;
    }
    if (!found) {
      var newData = [...list, data];
      await AsyncStorage.setItem(storeUsersListKey, JSON.stringify(newData));
      return true;
    }

    return false;
  } catch (error) {
    console.log('Error storing the storeUsersList', error);
  }
};

const updateUsersList = async (id, data) => {
  try {
    let list = JSON.parse(await AsyncStorage.getItem(storeUsersListKey));

    let found = _.find(list, function (i) {
      return i?.id == id;
    });
    if (found) {
      var filtered = list.filter(function (el) {
        return el.id != found?.id;
      });
      var update = { ...found, title: data };
      var newData = [...filtered, update];
      await AsyncStorage.setItem(storeUsersListKey, JSON.stringify(newData));
      return true;
    }
    return false;
  } catch (error) {
    console.log('Error storing the storeUsersList', error);
  }
};

const deleteUsersList = async (id) => {
  try {
    let list = JSON.parse(await AsyncStorage.getItem(storeUsersListKey));

    let found = _.find(list, function (i) {
      return i?.id == id;
    });
    if (found) {
      var filtered = list.filter(function (el) {
        return el.id != found?.id;
      });
      await AsyncStorage.setItem(storeUsersListKey, JSON.stringify(filtered));
      if (filtered?.length == 0) {
        return true;
      }
      return false;
    }
  } catch (error) {
    console.log('Error storing the storeUsersList', error);
  }
};

const storeUserDetails = async (data) => {
  try {
    await AsyncStorage.setItem(userDetails, JSON.stringify(data));
  } catch (error) {
    console.log('Error storing the storeUserDetails', error);
  }
};

const getUserDetails = async (data) => {
  try {
    let data = await AsyncStorage.getItem(userDetails);
    return JSON.parse(data);
  } catch (error) {
    console.log('Error storing the getUserDetails', error);
  }
};

const getLogin = async () => {
  try {
    return await AsyncStorage.getItem(login);
  } catch (error) {
    console.log('Error getting the BaseURL', error);
  }
};

const storeEventData = async (data) => {
  try {
    const jsonValue = JSON.stringify(data);
    await AsyncStorage.setItem(EventData, jsonValue);
  } catch (error) {
    console.log('Error storing the LoginData', error);
  }
};

const getEventData = async () => {
  try {
    return await AsyncStorage.getItem(EventData);
  } catch (error) {
    console.log('Error getting the auth token', error);
  }
};

const getToken = async () => {
  try {
    return await AsyncStorage.getItem(tokenKey);
  } catch (error) {
    console.log('Error getting the auth token', error);
  }
};

const storeBaseURL = async (data) => {
  try {
    let Data = await AsyncStorage.setItem(baseURL, JSON.stringify(data));
    settings.setBaseURL(data?.baseUrl);
    return Data;
  } catch (error) {
    console.log('Error storing the BaseURL', error);
  }
};

const storeBaseURLOnly = async (data) => {
  try {
    let Data = await AsyncStorage.setItem(baseURLOnly, data);
    return Data;
  } catch (error) {
    console.log('Error storing the baseURLOnly', error);
  }
};

const getBaseURLOnly = async (data) => {
  try {
    let Data = await AsyncStorage.getItem(baseURLOnly);
    return Data;
  } catch (error) {
    console.log('Error getting the baseURLOnly', error);
  }
};

const getBaseURL = async () => {
  try {
    return await AsyncStorage.getItem(baseURL);
  } catch (error) {
    console.log('Error getting the BaseURL', error);
  }
};

const getUser = async () => {
  const token = await getToken();
  return token ? jwtDecode(token) : null;
};

const getUserbyId = async (user) => {
  // const token = await getToken();
  return user ? jwtDecode(user) : null;
};

const removeToken = async () => {
  try {
    await AsyncStorage.removeItem(baseURL);
    await AsyncStorage.removeItem(tokenKey);
  } catch (error) {
    console.log('Error removing the auth token', error);
  }
};

export default {
  storeUsersList,
  getUsersList,
  deleteUsersList,
  updateUsersList,
  getUserbyId,
  storeUserDetails,
  getUserDetails,
  getUser,
  getLogin,
  storeLogin,
  storeBaseURLOnly,
  getBaseURLOnly,
  removeToken,
  storeToken,
  storeEventData,
  getEventData,
  getToken,
  storeBaseURL,
  getBaseURL,
};
