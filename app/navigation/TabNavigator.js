import React, { useContext, useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import HomeNavigator from './HomeNavigator';

import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import AllEventsScreen from '../screens/AllEventsScreen';
import SettingsScreen from '../screens/SettingsScreen';
import ProfileScreen from '../screens/ProfileScreen';
import colors from '../config/colors';
import EventNavigator from './EventNavigator';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  const Options = ({ iconName, label, navigation }) => {
    return {
      headerShown: false,
      tabBarLabelStyle: { color: colors.white, top: 10 },
      tabBarActiveTintColor: colors.redButtonColor,
      tabBarInactiveTintColor: colors.buttonBackground,
      tabBarLabel: label,
      tabBarIcon:
        label == 'Home'
          ? ({ color, size }) => (
              <View style={[styles.iconView, { backgroundColor: color }]}>
                <MaterialCommunityIcons name={iconName} color={colors.white} size={size} />
              </View>
            )
          : label == 'Settings'
          ? ({ color, size }) => (
              <View style={[styles.iconView, { backgroundColor: color }]}>
                <FontAwesome name={iconName} color={colors.white} size={size} />
              </View>
            )
          : ({ color, size }) => (
              <View style={[styles.iconView, { backgroundColor: color }]}>
                <Ionicons name={iconName} color={colors.white} size={size} />
              </View>
            ),
    };
  };

  const tabBarListeners = ({ navigation, route }) => ({
    tabPress: (e) => {
      if (route?.state && route?.state?.index > 0) {
        e.preventDefault();
        navigation.navigate('AllEventsScreen');
      }
    },
  });

  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: {
          backgroundColor: colors.buttonBackground,
          height: 80,
        },
        tabBarHideOnKeyboard: true,
        tabBarShowLabel: false,
      }}
    >
      <Tab.Screen
        name="Home"
        component={HomeNavigator}
        options={({ navigation }) =>
          Options({ iconName: 'home', label: 'Home', navigation: navigation })
        }
      />
      <Tab.Screen
        listeners={tabBarListeners}
        name="AllEvents"
        component={EventNavigator}
        options={() => Options({ iconName: 'scan', label: 'Scan' })}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={() => Options({ iconName: 'md-person-circle-outline', label: 'Profile' })}
      />
      <Tab.Screen
        name="Settings"
        component={SettingsScreen}
        options={() => Options({ iconName: 'power-off', label: 'Settings' })}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  iconView: {
    height: 29,
    width: 58,
    justifyContent: 'center',
    borderRadius: 77,
    alignItems: 'center',
  },
});

export default TabNavigator;
