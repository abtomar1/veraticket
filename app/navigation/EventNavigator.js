import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AllEventsScreen from '../screens/AllEventsScreen';
import EventScreen from '../screens/Events/EventScreen';
import EventScannerScreen from '../screens/Events/EventScannerScreen';
import EventDetailScreen from '../screens/Events/EventDetailScreen';

const Stack = createStackNavigator();

const EventNavigator = () => (
  <Stack.Navigator screenOptions={{ headerShown: false }}>
    <Stack.Screen name="AllEventsScreen" component={AllEventsScreen} />
    <Stack.Screen name="EventScreen" component={EventScreen} />
    <Stack.Screen name="EventScannerScreen" component={EventScannerScreen} />
    <Stack.Screen name="EventDetailScreen" component={EventDetailScreen} />
  </Stack.Navigator>
);

export default EventNavigator;
