import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AfterlogoutScreeen from '../screens/AfterlogoutScreeen';
import HowToScanScreen from '../screens/HowToScanScreen';
import ScanQRScreen from '../screens/ScanQRScreen';

const Stack = createStackNavigator();

const AfterLogoutNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="HowToScanScreen" component={HowToScanScreen} />
      <Stack.Screen name="AfterlogoutScreeen" component={AfterlogoutScreeen} />
      <Stack.Screen name="ScanQRScreen" component={ScanQRScreen} />
    </Stack.Navigator>
  );
};

export default AfterLogoutNavigator;
