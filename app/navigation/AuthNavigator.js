import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HowToScanScreen from '../screens/HowToScanScreen';
import ScanQRScreen from '../screens/ScanQRScreen';
import AddInformationScreen from '../screens/AddInformationScreen';
import InitialStepper from '../screens/InitialStepper';

const Stack = createStackNavigator();

const AuthNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="InitialStepper" component={InitialStepper} />
      <Stack.Screen name="HowToScanScreen" component={HowToScanScreen} />
      <Stack.Screen name="ScanQRScreen" component={ScanQRScreen} />
      <Stack.Screen name="AddInformationScreen" component={AddInformationScreen} />
    </Stack.Navigator>
  );
};

export default AuthNavigator;
