import store from '../auth/storage';

const getBaseUrl = async () => {
  let data = JSON.parse(await store.getBaseURL());
  return data.baseUrl;
};

export default getBaseUrl;
