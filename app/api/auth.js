import client from './client';
import { Platform } from 'react-native';

const signUp = (fullName, email, phone, numberOfBuildings, comments, source) =>
  client.post('/api/enroll', { fullName, email, phone, numberOfBuildings, comments, source });

export default {
  signUp,
};
