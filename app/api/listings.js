import client from './client';

const registartion = (data) => client.post(`/event-service/api/v1/scanner/registartion`, data);
const getEventData = () => client.get(`/event-service/api/v1/scanner/event`);
const getProfileData = (baseURL, id) => client.get(`/event-service/api/v1/scanner/get-scanner`);
const updateProfileData = (baseURL, id, data) =>
  client.put(`/event-service/api/v1/scanner/update-scanner`, data);
const getEventAnalytics = (baseURL, id) =>
  client.get(`/event-service/api/v1/scanner/event-analytics/${id}`);
const getQRDetails = (baseURL, id, data) =>
  client.post(`/event-service/api/v1/scanner/scan-qr-code/${id}`, data);
const getCurrentAppVersion = () => client.get(`/event-service/api/v1/application/app-version`);
const getAppLink = () => client.get(`/event-service/api/v1/application/app-url`);
const deLink = () => client.get(`/event-service/api/v1/scanner/delink`);
const getDeviceStatus = () => client.get(`/event-service/api/v1/application/device/status`);

export default {
  registartion,
  getEventData,
  getProfileData,
  updateProfileData,
  getEventAnalytics,
  getQRDetails,
  getCurrentAppVersion,
  getAppLink,
  deLink,
  getDeviceStatus,
};
