import { create } from 'apisauce';
import authStorage from '../auth/storage';

const apiClient = create({
  baseURL: '', // Here setting base Url
  headers: {
    'Content-Type': 'application/json',
  },
});

apiClient.addAsyncRequestTransform(async (request) => {
  const authToken = JSON.parse(await authStorage.getToken());
  if (!authToken) return;
  request.headers['x-api-token'] = authToken?.token;
});

export default apiClient;
